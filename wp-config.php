<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'kubet' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '&O^NLAWktpJ_~N%*7cUb-/=;0tYapz/|j6ccc/Wb~69D][ii(|12J0e`!3m|}c[1' );
define( 'SECURE_AUTH_KEY',  '^sVW9hi1Z(W|47iS{KuOhqPE0o-7i$.oVe3SIZYb!C>an&TEMBl(9L|MKGK(92CR' );
define( 'LOGGED_IN_KEY',    '<1(W/#ene{+lG1 1LztwFY-;AA6qQqvj1=(z*}}?W*3_~euIB%]0W;#$ejr@MqOI' );
define( 'NONCE_KEY',        'Z}xD~O=>F,:m>AusC|7_`;{M :*T6vBd8B}%u$lE?U.5^P&R[juhHy^ G-~O5V%7' );
define( 'AUTH_SALT',        'gq&MRV(eth*HmSL@%[.*?{zgWXe)3C}a%C11}4o!S^Z)2MQd=TjmHzl|@4NR&nL~' );
define( 'SECURE_AUTH_SALT', 's`}SL+}v@vD4nz?pC-l<mb8@DZULR!e|hOg`mkWR[H IB}Xz+:j}c{_57nI]i3[!' );
define( 'LOGGED_IN_SALT',   'Rdh=ZWh}XEkWAaC| ;4Rnow|fO6V%E-pGb^$_HZ->7BG.;PJH/zjYw,cw2i+e%F#' );
define( 'NONCE_SALT',       '`}N4k3d7C3Nc{S4&HCxNmR}-x4gpu+SXgK^J`16@ VLr8Za7)NR fP>mDt3|w57*' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

define( 'WP_AUTO_UPDATE_CORE', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';