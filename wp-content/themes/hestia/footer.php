
			<!-- <?php do_action( 'hestia_do_footer' ); ?> -->
			<?php if (!is_front_page() && !is_home()){ ?>
				<div class="container root_fter">
					<div class="wp_fter">
						<p class="p-o1">欢迎使用 <b>©MC俱乐部娱乐</b>服务</p>
						<p><a href="https://mc111.net/" target="_blank" style="color: #1acc8d;">MC俱乐部APP</a> ©2023版权所有</p>
					</div>
				</div>
			<?php } ?>
			<div class="footer-end" style="display: none;">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4 left-footer">
							<div class="lf-content-ft">
								<h2>Về chúng tôi</h2>
								<div class="text-lf">
									MC là sân chơi uy tín cung cấp cho người chơi rất nhiều các tựa đề game hấp dẫn như: Cá độ bóng đá, Lô đề, Casino trực tuyến... Các trò chơi được phát hiện bằng hệ thống nền tảng cá cược hiện đại và tiên tiến.
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-4 right-footer">
							<div class="wp-itemview">
								<h3>Liên hệ</h3>
								<ul>
									<li>Địa chỉ: 100 Lê Duẩn, Bến Nghé, Quận 1, Thành phố Hồ Chí Minh, Việt Nam</li>
									<li>Điện thoại: 0976283402</li>
									<li>Email: <span>mc@gmail.com</span></li>
									<li>Website: <span>https://mc.com</span></li>
									<li>Telegram: <span>@lucky27sky</span></li>
								</ul>
							</div>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-4 end-footer">
							<div class="lf-content-ft">
								<h2>Liên kết</h2>
								<ul class="iconxhoian">
									<!-- <li><a href="" target="_blank"><img src="/wp-content/uploads/images/fb.svg"></a></li>
									<li><a href="" target="_blank"><img src="/wp-content/uploads/images/in.svg"></a></li>
									<li><a href="" target="_blank"><img src="/wp-content/uploads/images/gg.svg"></a></li>
									<li><a href="" target="_blank"><img src="/wp-content/uploads/images/tw.svg"></a></li>
									<li><a href="" target="_blank"><img src="/wp-content/uploads/images/yt.svg"></a></li> -->
								</ul>
								<p class="copyright">© COPYRIGHT 2022 BY MC</p>
							</div>
						</div>
					</div>
					<div class="partner_ft">
						<!-- <img src="/wp-content/uploads/images/partner.png"> -->
					</div>
				</div>
			</div>
		</div>
	</div>
<?php wp_footer(); ?>
</body>
</html>