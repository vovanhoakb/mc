<?php
/**
 * The Blog Section
 *
 * @package Hestia
 */

/**
 * Class Hestia_Blog_Section
 */
class Hestia_Blog_Section extends Hestia_Abstract_Main {
	/**
	 * Initialize Blog Section
	 */
	public function init() {
		$this->hook_section();
	}

	/**
	 * Hook section in/
	 */
	private function hook_section() {
		$section_priority = apply_filters( 'hestia_section_priority', 60, 'hestia_blog' );
		add_action( 'hestia_sections', array( $this, 'do_section' ), absint( $section_priority ), 2 );
		add_action( 'hestia_do_blog_section', array( $this, 'render_section' ) );
	}

	/**
	 * Executes the hook on which the content is rendered.
	 */
	public function do_section() {
		do_action( 'hestia_do_blog_section', false );
	}

	/**
	 * Blog section content.
	 */
	public function render_section( $is_shortcode = false ) {

		/**
		 * Don't show section if Disable section is checked.
		 * Show it if it's called as a shortcode.
		 */
		$hide_section  = get_theme_mod( 'hestia_blog_hide', false );
		$section_style = '';
		if ( $is_shortcode === false && (bool) $hide_section === true ) {
			if ( is_customize_preview() ) {
				$section_style = 'style="display: none"';
			} else {
				return;
			}
		}

		/**
		 * Gather data to display the section.
		 */
		if ( current_user_can( 'edit_theme_options' ) ) {
			/* translators: 1 - link to customizer setting. 2 - 'customizer' */
			$hestia_blog_subtitle = get_theme_mod( 'hestia_blog_subtitle', sprintf( __( 'Change this subtitle in the %s.', 'hestia' ), sprintf( '<a href="%1$s" class="default-link">%2$s</a>', esc_url( admin_url( 'customize.php?autofocus&#91;control&#93;=hestia_blog_subtitle' ) ), __( 'Customizer', 'hestia' ) ) ) );
		} else {
			$hestia_blog_subtitle = get_theme_mod( 'hestia_blog_subtitle' );
		}
		$hestia_blog_title = get_theme_mod( 'hestia_blog_title', __( 'Blog', 'hestia' ) );
		if ( $is_shortcode ) {
			$hestia_blog_title    = '';
			$hestia_blog_subtitle = '';
		}

		/**
		 * In case this function is called as shortcode, we remove the container and we add 'is-shortcode' class.
		 */
		$wrapper_class   = $is_shortcode === true ? 'is-shortcode' : '';
		$container_class = $is_shortcode === true ? '' : 'container';

		$html_allowed_strings = array(
			$hestia_blog_title,
			$hestia_blog_subtitle,
		);
		maybe_trigger_fa_loading( $html_allowed_strings );

		hestia_before_blog_section_trigger(); ?>
		<section class="root-home">
			<div class="container bg_white_home">
				<!-- <div class="text_bg_home">
					<p><span>首存优惠活动</span></p>
				</div> -->


				<!-- <div class="root_article_home">
					<div class="item_home_article">
						<div class="row flex_mb">
							<div class="col-md-6 col-sm-6 col-xs-6 left_50">
								<div class="wp-tx">
									<h1>MC俱乐部</h1>
									<h2>全新房卡娱乐平台</h2>
									<h4>MC娱乐城房间号：334198</h4>
								</div>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-6 right_50">
								<div class="wp-img">
									<img src="/kubet/wp-content/uploads/images/h.png">
								</div>
							</div>
						</div>
					</div>
					<div class="item_home_article">
						<div class="text_bg_home">
							<p><span>首存优惠活动</span></p>
						</div>
						<h5 class="title_home_second">即日起会员首存金额达指定要求，即可获得对应首存红利，一倍流水即可提款。</h5>
						<img src="/kubet/wp-content/uploads/images/h4.png">
					</div>
					<div class="item_home_article">
						<div class="text_bg_home">
							<p><span>热门游戏</span></p>
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-6 left_50">
								<div class="wp-tx-h3">
									<h3><span>MC俱乐部是一款全新房卡娱乐平台APP</span>, 平台免费提供六合彩系列、澳洲系列、幸运飞艇、MC真人百家乐、时时彩系列等各类主流游戏，一应俱全，玩法丰富，用户体验丰富多彩，支持网投模式，微投模式，游戏内可以任意切换。</h3>
									<img src="/kubet/wp-content/uploads/images/h5.jpg">
								</div>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-6 right_50">
								<div class="wp-img">
									<img src="/kubet/wp-content/uploads/images/h6.png">
								</div>
							</div>
						</div>
					</div>
					<div class="item_home_article">
						<div class="text_bg_home">
							<p><span>MC俱乐部优势</span></p>
						</div>
						<div class="row">
							<div class="col-md-7 col-sm-7 col-xs-12 left_70">
								<div class="wp-tx-div">
									<div class="item-text">
										<div class="left_it">
											优势一：
										</div>
										<div class="right_it">
											MC俱乐部是一款全新房卡娱乐平台，一个APP即可享受不同房主提供的优质娱乐服务，全新娱乐游戏体验。
										</div>
									</div>
									<div class="item-text">
										<div class="left_it">
											优势二：
										</div>
										<div class="right_it">
											内置OEPay支付更便捷资金更安全，交易简单化，<span>使用OEPay复存入款笔笔赠送4%彩金无上限</span>
										</div>
									</div>
									<div class="item-text">
										<div class="left_it">
											优势三：
										</div>
										<div class="right_it">
											支付方式多样化，支持，微信，支付宝，银行卡，USDT，OEPay！总有一样适合你。
										</div>
									</div>
									<div class="item-text">
										<div class="left_it">
											优势四：
										</div>
										<div class="right_it">
											<span>香港六合彩48.6倍，时时彩赛车9.9倍超高倍率。</span>
										</div>
									</div>
									<div class="item-text">
										<div class="left_it">
											优势五：
										</div>
										<div class="right_it">
											MC俱乐部APP独创微投模式和网投模式两种娱乐模式。
										</div>
									</div>
									<div class="item-text">
										<div class="left_it">
											优势六：
										</div>
										<div class="right_it">
											<span>信誉大平台，千万提款秒到账，大额无忧。</span>
										</div>
									</div>
									<div class="item-text">
										<div class="left_it">
											优势七
										</div>
										<div class="right_it">
											<span>首存豪礼，打码反水，天降红包雨，</span>复存比比送超多福利活动等你来。
										</div>
									</div>
									<div class="item-text">
										<div class="left_it">
											优势八：
										</div>
										<div class="right_it">
											7X24小时365天在线客服随时为您服务，免去您的后顾之忧。
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-5 col-sm-5 col-xs-12 right_30">
								<div class="wp-img img-card_home">
									<img src="/kubet/wp-content/uploads/images/h7.png">
								</div>
							</div>
						</div>
					</div>
					<div class="item_home_article">
						<div class="text_bg_home">
							<p><span>OEPay支付</span></p>
						</div>
						<img src="/kubet/wp-content/uploads/images/h8.png">
					</div>
					<div class="item_home_article">
						<div class="text_bg_home">
							<p><span>信誉保障</span></p>
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-6 left_70">
								<div class="wp-tx-h6">
									<h6>MC俱乐部耗资千万新打造的首款房卡娱乐平台，我们始终牢记已诚信为本，信誉为根是我们立足的根本！所有房主均严格审核，确保他们都是诚实守信的经营者。<br /><span>MC娱乐城房间号：334198是MC俱乐部官方直营，安全可靠，还有超多豪礼，赶快来领取吧！</span></h6>
									<p>MC娱乐城房间号：334198</p>
								</div>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-6 right_30">
								<div class="wp-img">
									<img src="/kubet/wp-content/uploads/images/h3.png">
								</div>
							</div>
						</div>
					</div>
				</div> -->

				<!-- <div class="row row_promotion">
					<div class="col-md-4 col-sm-4 col-xs-4 item_promotion">
						<div class="wp-promotion">
							<h2>活动一</h2>
							<img src="/kubet/wp-content/uploads/images/km.png">
						</div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-4 item_promotion">
						<div class="wp-promotion">
							<h2>活动二</h2>
							<img src="/kubet/wp-content/uploads/images/km1.jpg">
						</div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-4 item_promotion">
						<div class="wp-promotion">
							<h2>活动三</h2>
							<img src="/kubet/wp-content/uploads/images/km2.jpg">
						</div>
					</div>
				</div> -->



				<!-- <div class="slider_usdt slider_itemlast" style="max-width: 850px;">
					<section class="slider-for center-slider">
					<div class="item_slick">
					<div class="img_slider_mobile"><img src="https://profx50.info/wp-content/uploads/2023/04/1-3.jpg" alt="" width="810" height="592" class="alignnone size-full wp-image-750"></div>
					</div>
					<div class="item_slick">
					<div class="img_slider_mobile"><img src="https://profx50.info/wp-content/uploads/2023/04/2-3.jpg" alt="" width="810" height="592" class="alignnone size-full wp-image-751"></div>
					</div>
					<div class="item_slick">
					<div class="img_slider_mobile"><img src="https://profx50.info/wp-content/uploads/2023/04/3-2.jpg" alt="" width="810" height="592" class="alignnone size-full wp-image-752"></div>
					</div>
					<div class="item_slick">
					<div class="img_slider_mobile"><img src="https://profx50.info/wp-content/uploads/2023/04/13.jpg" alt="" width="810" height="592" class="alignnone size-full wp-image-762"></div>
					</div>
					</section>
					<div class="popup_help_usdt">
						<div class="wp-usdt-open">
							<div class="popup_help_usdts">
								<h3>欧易网址无法开启？</h3>
								<p>1 请依照以下流程进行操作：</p>
								<p>2 打开最新版谷歌浏览器</p>
								<p>3 点击右上角三个点—“设置”</p>
								<p>4 选择“隐私设置和安全性”—“安全”</p>
								<p>点击“使用安全DNS”—“自定义”，完整输入 https://118.31.37.173/dns-query 复制</p>
								<p>以上步骤全部完成后即可访问官网，需要完整的网址： https://www.okx.com/ 复制</p>
								<span class="closeusdt">x</span>
							</div>
						</div>
					</div>
				</div>
					<script type="text/javascript" src="/kubet/wp-content/uploads/js/slick.js"></script>
					<script type="text/javascript">$(document).ready(function(){$('.center-slider').slick({slidesToShow: 1,slidesToScroll: 1,centerMode: true,arrows: true,dots: false,speed: 300,centerPadding: '0px',infinite: false,autoplaySpeed: 5000,autoplay: false});$('.btn_help_usdt').click(function(){$('.popup_help_usdt').show();});$('.closeusdt').click(function(){$('.popup_help_usdt').hide();});});</script> -->
				
				<!-- <div class="menu_usdt">
					<ul>
						<li class="active"><a href="/category/DepositCoinDataExample/">了解数字货币</a></li>
						<li>
							<a>欧易操作流程</a>
							<ul>
								<li><a href="/category/depositcoindataf1example/">注册帐号及认证</a></li>
								<li><a href="/category/depositcoindataf2example/">购买USDT</a></li>
								<li><a href="/category/depositcoindataf3example/">查看交易记录</a></li>
							</ul>
						</li>
						<li>
							<a>pexpay操作流程</a>
							<ul>
								<li><a href="/category/depositcoindatap1example/">注册帐号及认证</a></li>
								<li><a href="/category/depositcoindatap2example/">购买USDT</a></li>
								<li><a href="/category/depositcoindatap3example/">查看交易记录</a></li>
							</ul>
						</li>
					</ul>
				</div> -->
				<div class="content-home">
					<?php 
						$getposts = new WP_query(); $getposts->query('post_status=publish&showposts=1&cat=33'); 
						global $wp_query; $wp_query->in_the_loop = true;
						while ($getposts->have_posts()) : $getposts->the_post();
							echo the_content();
						endwhile; wp_reset_postdata(); 
					?>
				</div>
			</div>
			<?php dynamic_sidebar('block-after-content'); ?>
		</section>
		<section style="display: none;" class="hestia-blogs <?php echo esc_attr( $wrapper_class ); ?>" id="blog"
			data-sorder="hestia_blog" <?php echo wp_kses_post( $section_style ); ?>>
			<?php
			hestia_before_blog_section_content_trigger();
			if ( $is_shortcode === false ) {
				hestia_display_customizer_shortcut( 'hestia_blog_hide', true );
			}
			?>
			<div class="<?php echo esc_attr( $container_class ); ?>">
				<?php
				hestia_top_blog_section_content_trigger();
				if ( $is_shortcode === false ) {
					?>
					<div class="row">
						<div class="col-md-8 col-md-offset-2 text-center hestia-blogs-title-area">
							<?php
							hestia_display_customizer_shortcut( 'hestia_blog_title' );
							if ( ! empty( $hestia_blog_title ) || is_customize_preview() ) {
								echo '<h2 class="hestia-title">' . wp_kses_post( $hestia_blog_title ) . '</h2>';
							}
							if ( ! empty( $hestia_blog_subtitle ) || is_customize_preview() ) {
								echo '<h5 class="description">' . hestia_sanitize_string( $hestia_blog_subtitle ) . '</h5>';
							}
							?>
						</div>
					</div>
					<?php
				}
				?>
				<div class="hestia-blog-content">
				<?php
				$this->blog_content();
				?>
				</div>
				<?php hestia_bottom_blog_section_content_trigger(); ?>
			</div>
			<?php hestia_after_blog_section_content_trigger(); ?>
		</section>
		<?php
		hestia_after_blog_section_trigger();
	}

	/**
	 * Blog content/
	 */
	public function blog_content() {

		$hestia_blog_items      = get_theme_mod( 'hestia_blog_items', 3 );
		$args                   = array(
			'ignore_sticky_posts' => true,
		);
		$args['posts_per_page'] = ! empty( $hestia_blog_items ) ? absint( $hestia_blog_items ) : 3;

		$hestia_blog_categories = get_theme_mod( 'hestia_blog_categories' );

		if ( ! empty( $hestia_blog_categories[0] ) && sizeof( $hestia_blog_categories ) >= 1 ) {
			$args['tax_query'] = array(
				array(
					'taxonomy' => 'category',
					'field'    => 'term_id',
					'terms'    => $hestia_blog_categories,
				),
			);
		}

		$loop = new WP_Query( $args );

		$allowed_html = array(
			'br'     => array(),
			'em'     => array(),
			'strong' => array(),
			'i'      => array(
				'class' => array(),
			),
			'span'   => array(),
		);

		if ( ! $loop->have_posts() ) {
			return;
		}
			$i = 1;
			echo '<div class="row" ' . hestia_add_animationation( 'fade-up' ) . '>';
		while ( $loop->have_posts() ) :
			$loop->the_post();
			?>
			<article class="col-xs-12 col-ms-10 col-ms-offset-1 col-sm-8 col-sm-offset-2 <?php echo esc_attr( apply_filters( 'hestia_blog_per_row_class', 'col-md-4' ) ); ?> hestia-blog-item">
				<div class="card card-plain card-blog">
					<?php if ( has_post_thumbnail() ) : ?>
							<div class="card-image">
								<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
									<?php the_post_thumbnail( 'hestia-blog' ); ?>
								</a>
							</div>
						<?php endif; ?>
					<div class="content">
						<h6 class="category"><?php echo hestia_category(); ?></h6>
						<h4 class="card-title entry-title">
							<a class="blog-item-title-link" href="<?php echo esc_url( get_permalink() ); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark">
								<?php echo wp_kses( force_balance_tags( get_the_title() ), $allowed_html ); ?>
							</a>
						</h4>
						<p class="card-description"><?php echo wp_kses_post( get_the_excerpt() ); ?></p>
					</div>
				</div>
				</article>
				<?php
				if ( $i % apply_filters( 'hestia_blog_per_row_no', 3 ) === 0 ) {
					echo '</div><!-- /.row -->';
					echo '<div class="row" ' . hestia_add_animationation( 'fade-up' ) . '>';
				}
				$i++;
			endwhile;
			echo '</div>';

			wp_reset_postdata();
	}

}